<?php

class Location {

    private $numero_resa;
    private $client;
    private $voiture;
    private $date_resa;
    private $date_depart;
    private $date_retour;

    public function __construct($numero_resa_constr, $client_constr, $voiture_constr, $date_resa_constr, $date_depart_constr, $date_retour_constr){
        $this->numero_resa=$numero_resa_constr;
        $this->client=$client_constr;
        $this->voiture=$voiture_constr;
        $this->date_resa=$date_resa_constr;
        $this->date_depart=$date_depart_constr;
        $this->date_retour=$date_retour_constr;
    }


    public function getnumero_resa(){
        return $this->numero_resa;
    }
    public function setnumero_resa($numero_resa_saisie){
        $this->numero_resa = $numero_resa_saisie;
    }

    public function getclient(){
        return $this->client;
    }
    public function setclient($client_saisie){
        $this->client = $client_saisie;
    }

    public function getvoiture(){
        return $this->voiture;
    }
    public function setvoiture($voiture_saisie){
        $this->voiture = $voiture_saisie;
    }

    public function getdate_resa(){
        return $this->date_resa;
    }
    public function setdate_resa($date_resa_saisie){
        $this->date_resa = $date_resa_saisie;
    }

    public function getdate_depart(){
        return $this->date_depart;
    }
    public function setdate_depart($date_depart_saisie){
        $this->date_depart = $date_depart_saisie;
    }

    public function getdate_retour(){
        return $this->date_retour;
    }
    public function setdate_retour($date_retour_saisie){
        $this->date_retour = $date_retour_saisie;
    }

}
