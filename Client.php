<?php

    class Client {

        private $id;
        private $nom;
        private $prenom;
        private $adresse;
        private $code_postal;
        private $ville;
        private $telephone;
        private $email;
        private $date_naissance;
        private $numero_permis;
        private $date_permis;

        public function __construct($nom_constr,$prenom_constr, $telephone_constr, $email_constr) {
            $this->nom=$nom_constr;
            $this->prenom=$prenom_constr;
            $this->telephone=$telephone_constr;
            $this->email=$email_constr;
        }

    }

?>