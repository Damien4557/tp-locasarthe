<?php
 
    class Type{
    
    private $id_types;
    private $libelle;
    
    
 
     public function __construct($id_types, $libelle){
     
     $this->id_types = $id_types;
     $this->libelle = $libelle;
    
    }

 /**
     * Récupérer la valeur de l'attribut $id_types & $libelle
     */
    function getid(){
        return $this->id_types;
    }

    function getlibelle(){
        return $this->libelle;
    }
}